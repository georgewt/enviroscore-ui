import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Search: {
            screens: {
              SearchScreen: 'one',
            },
          },
          Form: {
            screens: {
              FormScreen: 'two',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
