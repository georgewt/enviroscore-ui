import * as React from 'react';
import { StyleSheet, Picker } from 'react-native';

import { Text, View } from '../components/Themed';

export default function SearchScreen() {
  const [select, setSelect] = React.useState("cafe");
  const [locationList, setLocationList] = React.useState<any[]>();
  var places:any[] = [];
  const latitude = 38.0293;
  const longitude = -78.4767;
  const radius = 15000;
  const url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" +
               latitude + "," + longitude + 
               "&radius=" + radius + 
               "&type=" + "cafe" +
               "&key=AIzaSyAv2Yn_-dHUBfpX_mEIKFtAREMbPYbrRsk";
  
  fetch(url).then(res => {
    //console.log("response: " + JSON.stringify(res.body));
    return res.json();
  }).then(res => {
      //console.log("HELLO");
      for (let googlePlace of res.results) {
        //console.log("yo")
        var place = {
          coordinate: {
            latitude: 0,
            longitude: 0,
          },
          placeName: ""
        };
        var myLat = googlePlace.geometry.location.lat;
        var myLong = googlePlace.geometry.location.lng;
        var coordinate = {
          latitude: myLat,
          longitude: myLong,
        };
        place['coordinate'] = coordinate;
        place['placeName'] = googlePlace.name;
        //console.log("placename: " + place['placeName'])
        places.push(place);
      }
      setLocationList(places);
  })

  //console.log("locale: " + locationList)

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Search</Text>
      <Picker
        selectedValue={select}
        style={styles.dropdown}
        onValueChange={(itemValue) => setSelect(itemValue)}
      >
        <Picker.Item label="Bakery" value="bakery"/>
        <Picker.Item label="Bar" value="bar"/>
        <Picker.Item label="Salon" value="beauty_salon"/>
        <Picker.Item label="Book Store" value="book_store"/>
        <Picker.Item label="Cafe" value="cafe"/>
        <Picker.Item label="Clothing Store" value="clothing_store"/>
        <Picker.Item label="Department Store" value="department_store"/>
        <Picker.Item label="Florist" value="florist"/>
        <Picker.Item label="Gym" value="gym"/>
        <Picker.Item label="Laundromat" value="laundry"/>
        <Picker.Item label="Pet Shop" value="pet_store"/>
        <Picker.Item label="Restaurant" value="restaurant"/>
        <Picker.Item label="Spa" value="spa"/>
      </Picker>
      {locationList?.map((item, idx) => {
        
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  dropdown: {
    width: '60%'
  }
});
