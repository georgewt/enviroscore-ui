export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Search: undefined;
  Form: undefined;
};

export type SearchParamList = {
  SearchScreen: undefined;
};

export type FormParamList = {
  FormScreen: undefined;
};
