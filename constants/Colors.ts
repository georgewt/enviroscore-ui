const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';
const darkSeaGreen =  '#7cb895';
const tintColorGreen = '#49a684';
const steelBlue = '#427eb0';
const tintColorBlue = '#295083';

export default {
  light: {
    text: '#fff',
    background: darkSeaGreen,
    tint: tintColorGreen,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorGreen,
  },
  dark: {
    text: '#fff',
    background: steelBlue,
    tint: tintColorBlue,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorBlue,
  },
};
